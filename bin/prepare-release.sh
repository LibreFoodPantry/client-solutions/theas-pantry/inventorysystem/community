#!/bin/sh
set -e

VERSION="$1"

mkdir -p ./artifacts/release
sed -r "s/version: [0-9]+\.[0-9]+\.[0-9]+/version: $VERSION/" \
    specification/openapi.yaml > ./artifacts/release/openapi.yaml
